{
    "name": "l10n_mx_transport_document",
    "version": "16.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "account",
        "l10n_mx_edi",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # data
        "data/sat_transport_catalog.xml",
        "data/sat_trailer_catalog.xml",
        "data/sat_config_transport_catalog.xml",
        "data/sat_permSCT_catalog.xml",
        "data/sat_package_catalog.xml",
        "data/sat_warning_material_catalog.xml",
        "data/sat_arancel.xml",
        "data/sat_code_cp_catalog.xml",
        "data/sat_transport_parts_catalog.xml",
        # reports
        # views
        "views/res_partner.xml",
        "views/account_move.xml",
        "views/transport_complement_ubication.xml",
        "views/transport_complement_items.xml",
        "views/sat_code_cp_catalog.xml",
        "views/sat_config_transport_catalog.xml",
        "views/transport_item_ubication.xml",
    ],
}
