from asyncio import FastChildWatcher
from asyncio.log import logger
import logging
from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import re

_logger = logging.getLogger(__name__)


class Transport_Complement_Item_Ubication(models.Model):
    _name="l10n_mx_transport_complement_item_ubication"

    quantity = fields.Float(
        digits=(12,6),
        string="Cantidad",
        required=True,
    )
    destiny_code = fields.Char(
        required=True,
        string="ID Destino",
    )
    origin_code = fields.Char(
        required=True,
        string="ID Origen",
    )
    item_id = fields.Many2one(
        comodel_name="l10n_mx_transport_complement_items",
    )
    

