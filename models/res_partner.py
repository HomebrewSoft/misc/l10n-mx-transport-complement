from odoo import fields, models, api, _, tools
from odoo.exceptions import ValidationError 

import re

class ResPartner(models.Model):
    _inherit = "res.partner"

    add_transport_complement = fields.Boolean(
        string="Añadir Complemento Carta Porte",
    )
    add_transport_data_complement = fields.Boolean(
        string="Tipo Figura",
    )
    numLicense = fields.Char()
    num_regidtrib = fields.Char()
    transport_property_id = fields.Many2one(
        comodel_name="sat_transport_parts_catalog",
    )
    carrier_type = fields.Selection(
        [
            ("Operador", "Operador"),
            ("Propietario", "Propietario"),
            ("Notificado","Notificado"),
            ("Arrendedor","Arrendedor"),
        ],
    )

    @api.constrains("numLicense")
    def check_numLicense(self):
        if self.numLicense and self.carrier_type == "Operador":
            pattern = "[^(?!.*\s)-]{6,16}"
            if re.match(pattern, self.numLicense):
                return True
            else:
                raise ValidationError(
                    _(
                        "CCT:NumLicencia - Solo se permiten caracteres alfanúmericos y debe contener una longitud de 6 a 16."
                    )
                )
        return False
    
    @api.constrains("carrier_type")
    def validate_carrier_type(self):
        if self.carrier_type == "Operador" and not self.numLicense:
            raise ValidationError(
                _(
                    "CP:NumLicencia - Favor de ingresar información en el campo Número de Licencia."
                )
            )
        else:
            if self.carrier_type == "Propietario" and not self.transport_property_id:
                raise ValidationError(
                    _(
                        "CP:PartesTransporte - Favor de seleccionar una opción del catálogo de Partes Transporte."
                    )
                )
        return True


    @api.onchange("add_transport_data_complement")
    def change_distance(self):
        if self.add_transport_data_complement == False and self.carrier_type :
            self.carrier_type = ""
    
    
