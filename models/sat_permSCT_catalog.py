import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."

class sat_permSCT_catalog(models.Model):
    _name = "sat_permsct_catalog"
    _description = "Catálogo de permisos del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave del Tipo de Remolque debe ser único !"),
    ]

    code = fields.Char(
        string="Clave tipo permiso",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    transport_code = fields.Char(
        string="Clave transporte",
        required=True,
        default="2021-06-01",
    )
    start_date = fields.Date(
        string="Fecha de inicio de vigencia",
        required=False,
        default="2021-06-01",
    )
    end_date = fields.Date(
        string="Fecha de fin de vigencia",
        required=False,
    )