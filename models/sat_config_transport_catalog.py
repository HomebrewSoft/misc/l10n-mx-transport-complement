import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."


class sat_config_transport_catalog(models.Model):
    _name = "sat_config_transport_catalog"
    _description = "Catálogo de configuración autotransporte federal del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave de Configuración Autotransporte Federal debe ser único !"),
    ]

    code = fields.Char(
        string="Clave tipo remolque",
        required=True,
    )
    name = fields.Char(
        string="Descripción",
        required=True,
    )
    description = fields.Char(
        string="Descripción detallada",
    )
    trailer = fields.Char(
        string="Remolque",
        required=True,
    )
    start_date = fields.Date(
        string="Vigencia Inicio",
        required=False,
        default="2021-06-01",
    )