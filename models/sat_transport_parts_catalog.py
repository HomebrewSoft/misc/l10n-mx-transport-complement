import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."


class sat_transport_parts_catalog(models.Model):
    _name = "sat_transport_parts_catalog"
    _description = "Claves de Partes del Transporte rentadas o prestadas del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave de la Parte del Transporte rentada o prestada debe ser único !"),
    ]

    code = fields.Char(
        string="Clave Parte del Transporte",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    start_date = fields.Date(
        string="Vigencia Inicio",
        required=False,
        default="2021-12-01",
    )
    end_date = fields.Date(
        string="Vigencia Fin",
        required=False,
    )