from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import tempfile
import jinja2
import os
import unicodedata
import re

class AccountMoveLine(models.Model):
    _inherit = "account.move.line"

    weight_kg = fields.Float(
        digits=(12,3),
    )
    commerce_uuid = fields.Char(
        size=31
    )
    dimensions = fields.Char()



    



