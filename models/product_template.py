from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import tempfile
import jinja2
import os
import unicodedata
import re

class ProductTemplate(models.Model):
    _inherit = "product.template"

    property_carried = fields.Many2one(comodel_name="product.product")
    tariff_id = fields.Many2one(comodel_name="sat_arancel")
    warning_material_id = fields.Many2one(comodel_name="sat_warning_material_catalog")
    packaging_id = fields.Many2one(comodel_name="sat_package_catalog")
    is_warning = fields.Boolean()



    



