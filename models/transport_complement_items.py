from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import re
import logging

_logger = logging.getLogger(__name__)

class Transport_Complement_Items(models.Model):
    _name="l10n_mx_transport_complement_items"

    name = fields.Char(
        string="Descripción",
        required=True,
    )
    code_sat_CP_id = fields.Many2one(
        required=True,
        string="Bienes Transportados",
        comodel_name="sat_code_cp_catalog",
    )
    quantity = fields.Float(
        digits=(12,6),
        string="Cantidad",
        required=True,
    )
    code_sat_uom_id = fields.Many2one(
        required=True,
        comodel_name="product.unspsc.code",
        string="Clave Unidad CP",
        domain=[
            (
                "applies_to",
                "=",
                ["uom"],
            )
        ],
    )
    warning_material_id = fields.Many2one(
        comodel_name="sat_warning_material_catalog",
        string="Material Peligroso",
    )
    packaging_id = fields.Many2one(
        comodel_name="sat_package_catalog",
        string="Embalaje",
    )
    kg_weigth = fields.Float(
        digits=(12,3),
        required=True,
        string="Peso en KG",
    )
    merch_value = fields.Float(
        digits=(12,3),
        string="Valor Mercancia",
    )
    move_id_fk = fields.Many2one(
        comodel_name="account.move",
    )
    is_warning_material = fields.Boolean(
        store=True,
        compute="_compute_warning",
    )
    item_ubication_ids = fields.One2many("l10n_mx_transport_complement_item_ubication","item_id")

    @api.constrains("kg_weigth")
    def validate_kg(self):
        for items_move in self:
            if items_move.kg_weigth == 0.000:
                raise ValidationError(
                    _(
                        "CP:PesoKG - El peso de la mercancia debe ser mayor a cero. Favor de revisar la información."
                    )
                )
    
    @api.constrains("quantity")
    def validate_quantity(self):
        for items_move in self:
            if items_move.quantity == 0.000000:
                raise ValidationError(
                    _(
                        "CP:Cantidad - La cantidad de mercancia a trasladar debe ser mayor a cero. Favor de revisar la información."
                    )
                )

    @api.constrains("code_sat_CP_id")
    def validate_warning_material(self):
        for items_move in self:
            if items_move.code_sat_CP_id.warning == "1":
                if not items_move.warning_material_id:
                    raise ValidationError(
                        _(
                            "CP:MaterialPeligroso - La clave de producto seleccionada indica que se trata de un materia peligroso. Favor de seleccionar un valor del catálogo."
                        )
                    )
                if not items_move.packaging_id:
                    raise ValidationError(
                        _(
                            "CP:Embalaje - Falta información en el campo Embalaje. Favor de seleccionar un valor del catálogo."
                        )
                    )

    @api.depends("code_sat_CP_id")
    def _compute_warning(self):
        for item in self:
            if item.code_sat_CP_id:
                if item.code_sat_CP_id.warning == "0":
                    item.is_warning_material = False
                else:
                    item.is_warning_material = True
            else:
                item.is_warning_material = False
