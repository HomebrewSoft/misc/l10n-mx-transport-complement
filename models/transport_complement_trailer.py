from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError
from odoo.osv import expression


import re

class Transport_Complement_Trailer(models.Model):
    _name="l10n_mx_transport_complement_trailer"

    name = fields.Char(
        string="Descripción",
        required=True,
    )
    subtype = fields.Many2one(
        comodel_name="sat_trailer_catalog",
        string="Subtipo Remolque",
        required=True,
    )
    trailer_license = fields.Char(
        string="Placas",
        required=True,
    )

    @api.constrains("trailer_license")
    def _check_trailer_license(self):
        if self.trailer_license:
            pattern = "[^(?!.*\s)-]{6,7}"
            if re.match(pattern, self.trailer_license):
                return True
            else:
                raise ValidationError(
                    _(
                        "CCT:Placa - Solo se permiten caracteres alfanúmericos y debe contener una longitud de 6 a 7."
                    )
                )
        return False
    
