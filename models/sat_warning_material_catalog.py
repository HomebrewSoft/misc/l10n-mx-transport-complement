import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."

class sat_warning_material_catalog(models.Model):
    _name = "sat_warning_material_catalog"
    _description = "Catálogo de materiales peligrosos del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave del material peligroso ser único !"),
    ]

    code = fields.Char(
        string="Clave material peligroso",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    start_date = fields.Date(
        string="Fecha de inicio de vigencia",
        required=False,
        default="2021-06-01",
    )
    end_date = fields.Date(
        string="Fecha de fin de vigencia",
        required=False,
    )