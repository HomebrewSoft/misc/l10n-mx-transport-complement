import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."

class sat_package_catalog(models.Model):
    _name = "sat_package_catalog"
    _description = "Catálogo de tipo de embalaje del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave del Tipo de Embalaje debe ser único !"),
    ]

    code = fields.Char(
        string="Clave tipo embalaje",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    start_date = fields.Date(
        string="Fecha de inicio de vigencia",
        required=False,
        default="2021-06-01",
    )
    end_date = fields.Date(
        string="Fecha de fin de vigencia",
        required=False,
    )