from asyncio import FastChildWatcher
from asyncio.log import logger
import logging
from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import re

_logger = logging.getLogger(__name__)


class Transport_Complement_Ubication(models.Model):
    _name="l10n_mx_transport_complement_ubication"
    

    date_time_transport = fields.Datetime(
        required=True,
    )
    date_time_transport_fomart = fields.Char()
    delivery_departure_point = fields.Many2one(
        required=True,
        comodel_name="res.partner",
    )
    address = fields.Char(
        string="Domicilio",
        compute="_compute_address",
        store=True,
    )
    ubication_type = fields.Selection(
        [
            ("Origen", "Origen"),
            ("Destino", "Destino"),
        ],
        required=True,
    )
    distance = fields.Float(
        digits=(5,2),
    )
    account_move_fk = fields.Many2one(
        comodel_name="account.move",
        readonly=True,
    )
    invoice_partner = fields.Char(
        related="account_move_fk.partner_id.name",
    )
    invoice_company = fields.Char(
        related="account_move_fk.company_id.name",
    )
    partner_name = fields.Char()
    code_ubication = fields.Char(
        size=6,
    )
    client_address = fields.Boolean()
    
    
    @api.constrains("ubication_type")
    def validate_distance(self):
        for ubication_move in self:
            if ubication_move.ubication_type == "Destino" and ubication_move.distance == 0.00:
                raise ValidationError(
                    _(
                        "CP:Distancia - El valor en el campo Distancia debe ser mayor a cero."
                    )
                )
        return True   

    @api.onchange("ubication_type")
    def change_distance(self):
        if self.ubication_type == "Origen" :
            self.distance = 0.00
            self.partner_name = self.invoice_company
        else:
            if self.ubication_type == "Destino":
                self.partner_name = self.invoice_partner
    
    @api.onchange("delivery_departure_point")
    def _compute_address(self):
        if self.delivery_departure_point and self.delivery_departure_point != "":
            self.address = self.delivery_departure_point.street_name + str(self.delivery_departure_point.street_number) + "," + self.delivery_departure_point.country_id.name
        else:
            self.address = ""
     
    @api.onchange("code_ubication")
    def check_digit(self):
        if self.code_ubication:
            for code in self.code_ubication:
                if not code.isdigit():
                    raise ValidationError("CCP:IdUbicación - Solo se permiten números.")
            if len(self.code_ubication) < 6:
                raise ValidationError("CCP:IdUbicación - El ID debe contener 6 dígitos.")

    @api.onchange("client_address")
    def change_address(self):
        self.delivery_departure_point = False
        if self.client_address:
            self.partner_name = self.invoice_partner
        else:
            self.partner_name = self.invoice_company