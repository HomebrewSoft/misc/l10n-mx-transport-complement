import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."


class sat_arancel(models.Model):
    _name = "sat_arancel"
    _description = "Fracciones arancelarias del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave de Fracción Arancelaria debe ser única !"),
    ]

    code = fields.Char(
        string="Clave de Fracción Arancelaria",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    start_date = fields.Date(
        string="Vigencia Inicio",
        required=False,
        default="2021-12-01",
    )
    end_date = fields.Date(
        string="Vigencia Fin",
        required=False,
    )
    umt = fields.Char(
        string="UMT",
        required=False,
    )