import logging

from odoo import api, fields, models
from odoo.osv import expression

_logger = logging.getLogger(__name__)

msg2 = "Contacta a tu administrador de Sistema ."


class sat_code_cp_catalog(models.Model):
    _name = "sat_code_cp_catalog"
    _description = "Catálogo de Claves Servicios y/o Productos del SAT"

    _sql_constraints = [
        ("code_uniq", "UNIQUE(code)", "La clave del Servicio y/o Producto debe ser único !"),
    ]

    code = fields.Char(
        string="Clave tipo remolque",
        required=True,
    )
    name = fields.Char(
        string="Descripcion",
        required=True,
    )
    warning = fields.Char(
        string="¿Material peligroso?",
        required=True
    )
    start_date = fields.Date(
        string="Vigencia Inicio",
        required=False,
        default="2021-06-01",
    )
    end_date = fields.Date(
        string="Vigencia Fin",
        required=False,
    )