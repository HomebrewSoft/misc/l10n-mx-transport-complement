from jinja2.runtime import Context
from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError, UserError
from markupsafe import Markup
from lxml import etree
from xml.etree import ElementTree as ET

import pytz
import base64
import tempfile
import jinja2
import os
import unicodedata
import re
import logging
import time
import datetime

_logger = logging.getLogger(__name__)

class AccountMove(models.Model):
    _inherit = "account.move"

    country_dest_orig = fields.Many2one(comodel_name="res.country")

    add_transport_complement = fields.Boolean(
        related="partner_id.add_transport_complement",
    )
    cfdi_transport_complement = fields.Boolean()
    transpInter = fields.Boolean()
    estadoMerc = fields.Selection(
        [
            ("Entrada", "Entrada"),
            ("Salida", "Salida"),
        ],
        string="Entrada o Salida Mercancias",
    )
    viaMerc = fields.Many2one(comodel_name="sat_transport_catalog")


    ubication_ids = fields.One2many(
        "l10n_mx_transport_complement_ubication", "account_move_fk"
    )
    items_ids = fields.One2many(
        "l10n_mx_transport_complement_items", "move_id_fk"
    )
    vehicle_ids = fields.Many2one(
        comodel_name="l10n_mx_transport_complement_vehicle"
    )
    trailer_ids = fields.Many2many(
        comodel_name="l10n_mx_transport_complement_trailer"
    )

    operator_ids = fields.Many2one(
        comodel_name="res.partner",
        domain=[
            (
                "carrier_type",
                "in",
                ["Operador", "Notificado", "Propietario", "Arrendador"],
            )
        ],
        string="Figuras Transporte",
    )

    trailer_required = fields.Boolean(
      store=True,
      compute="_trailer_required",
    )

    @api.depends("vehicle_ids")
    def _trailer_required(self):
        if self.vehicle_ids.vehicleConfig.trailer == "0":
            self.trailer_required = False
        else:
            self.trailer_required = True

    @api.onchange("trailer_ids")
    @api.constrains("trailer_ids")
    def validate_trailers(self):
        if len(self.trailer_ids) > 2:
            raise ValidationError("Solo se permiten dos remolques")

    def _get_transport_complement_content(invoice):
        invoice.ensure_one()
        data = {}
        template = "l10n-mx-transport-complement/data/template/carta_porte.xml.jinja"
        _logger.info("template" + template)
        (fileno_xml, fname_xml) = tempfile.mkstemp(".xml", "odoo_" + "__facturae_pay__")
        transp_inter = "Si" if invoice.transpInter else "No"
        totalkg = sum(line.kg_weigth for line in invoice.items_ids)
        totaldistance = sum(ubication.distance for ubication in invoice.ubication_ids)
        totalmercancias = len(invoice.items_ids)
        mercancias = invoice.items_ids
        if not data:
            data = {
                "move": invoice,
                "version": "2.0",
                "ubications": invoice.ubication_ids,
                "totaldistance": totaldistance,
                "transp_inter": transp_inter,
                "emisor": invoice.company_id,
                "receptor": invoice.partner_id,
                "mercancias": mercancias,
                "vehicle": invoice.vehicle_ids,
                "trailers": invoice.trailer_ids,
                "total_mercancias": totalmercancias,
                "totalkg": totalkg,
                "operators": invoice.operator_ids,
            }
        with tools.file_open(template, "r") as f_jinja_tmpl:
            jinja_tmpl_str = f_jinja_tmpl.read()
            tmpl = jinja2.Template(jinja_tmpl_str)
            with open(fname_xml, "w") as new_xml:
                new_xml.write(tmpl.render(**data))
                new_xml.close()
        with open(fname_xml, "rb") as b:
            data_xml = b.read()
        b.close()
        # date_formated = data['ubications'].date_time_transport.strftime('%Y-%m-%dT%H:%M:%S')
        # original_date = data['ubications'].date_time_transport.strftime('%Y-%m-%d %H:%M:%S')
        # data_xml = data_xml.decode().replace(original_date, date_formated).encode()
        return data_xml

    def generate_transport_complement(self, invoice, res):

        attachment_content = res['cfdi_str']
        attachment_content = attachment_content.decode('utf-8')

        if attachment_content.find("</cfdi:Complemento>") == -1:
            attachment_content = attachment_content.replace(
                "</cfdi:Comprobante>",
                "<cfdi:Complemento></cfdi:Complemento></cfdi:Comprobante>",
            )
        if attachment_content.find('xmlns:cce11="http://www.sat.gob.mx/ComercioExterior11"') != -1:
            attachment_content = attachment_content.replace(
                'xmlns:cce11="http://www.sat.gob.mx/ComercioExterior11"',
                'xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cartaporte20="http://www.sat.gob.mx/CartaPorte20"'
            )

        if attachment_content.find('xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/ComercioExterior11 http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior11/ComercioExterior11.xsd"') != -1:
            attachment_content = attachment_content.replace(
                'xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/ComercioExterior11 http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior11/ComercioExterior11.xsd"',
                'xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/CartaPorte20 http://www.sat.gob.mx/sitio_internet/cfd/CartaPorte/CartaPorte20.xsd"'
            )
        ccc = AccountMove._get_transport_complement_content(invoice).decode()
        ccc = (
            ccc.replace("  ", "")
            .replace("\n", "")
            .replace("                  ", "")
            .replace("          ", "")
        )
        if ccc:
            # replace in variable bits_attachment_content type bytes the string </cfdi:Complemento> with the content of ccc type string
            xml_data = attachment_content.replace(
                "</cfdi:Complemento>",
                f"{ccc}</cfdi:Complemento>"
            )

            # remove from xml_data <?xml version='1.0' encoding='UTF-8'?>\n
            cfdi_values = self._l10n_mx_edi_get_invoice_cfdi_values(invoice)
            qweb_template="l10n_mx_edi_40.cfdiv40"
            xsd_attachment_name="cfdv40.xsd"
            xml_data_to_encode = xml_data.replace("<?xml version='1.0' encoding='UTF-8'?>\n", "")
            cfdi = Markup(xml_data_to_encode)
            decoded_cfdi_values = invoice._l10n_mx_edi_decode_cfdi(cfdi_data=cfdi)
            cfdi_cadena_crypted = cfdi_values['certificate'].sudo()._get_encrypted_cadena(decoded_cfdi_values['cadena'])
            decoded_cfdi_values['cfdi_node'].attrib['Sello'] = cfdi_cadena_crypted

            # res = {
            #     'cfdi_str': etree.tostring(decoded_cfdi_values['cfdi_node'], pretty_print=True, xml_declaration=True,
            #                                encoding='UTF-8'),
            # }
            try:
                self.env['ir.attachment'].l10n_mx_edi_validate_xml_from_attachment(decoded_cfdi_values['cfdi_node'],
                                                                                   xsd_attachment_name)
            except UserError as error:
                res['errors'] = str(error).split('\\n')
            # for xml_data string replace the content of Sello with cfdi_cadena_crypted, identify the sello text by
            # searching the string Sello=" and replace the content until the next "
            current_sello = xml_data.split('Sello="')[1].split('"')[0]
            # convert cfdi_cadena_crypted to string
            cfdi_cadena_crypted = cfdi_cadena_crypted.decode()
            # replace Sello in xml_data with cfdi_cadena_crypted
            xml_data = xml_data.replace(
                f"Sello=\"{current_sello}\"",
                f"Sello=\"{cfdi_cadena_crypted}\""
            )
            res['cfdi_str'] = xml_data.encode()
        else:
            raise ValidationError(_("Not data to generate CFDI with complement"))
            
    def validate_ubications(self, ubications):
        if len(ubications) < 2:
            raise ValidationError(
                _("CP:Ubicaciones - Es necesario contar con al menos dos ubicaciones. ")
            )
        else:
            origin_count = 0
            destiny_count = 0
            for ubication in ubications:
                tz = self.env.user.partner_id.tz or "Mexico/General"
                dt = self.server_to_local_timestamp(ubication.date_time_transport, tz)
                dt_format = dt.strftime("%Y-%m-%dT%H:%M:%S")
                ubication.date_time_transport_fomart = dt_format
                if ubication.ubication_type == "Origen":
                    origin_count += 1
                else:
                    destiny_count += 1
            if destiny_count < 1 or origin_count < 1:
                raise ValidationError(
                    _(
                        "CP:Ubicaciones - Se debe contar con una Ubicación Origen y Destino."
                    )
                )

    def validate_items(self, items):
        if len(items) < 1:
            raise ValidationError(
                _(
                    "CP:Mercancias - Se debe registrar al menos una mercancia en el apartado de Mercancias."
                )
            )

    def validate_personal_transport(self, employees):
        if len(employees) < 1:
            raise ValidationError(
                _(
                    "CP:FiguraTransporte - Se debe registrar al menos una Figura Transporte."
                )
            )
        else:
            operators_count = 0
            for employee in employees:
                if employee.carrier_type == "Operador":
                    operators_count += 1
            if operators_count == 0:
                raise ValidationError(
                    _(
                        "CP:FiguraTransporte - No se encuentra ninguna Figura Transporte que corresponda al tipo Operador."
                    )
                )

    def validate_vehicle(self, vehicle):
        if not vehicle:
            raise ValidationError(
                _("CP:Vehiculo - No se registro información sobre la unidad vehicular.")
            )

    def post(self):
        move_obj = self.env["account.move"]
        for acct_move in self:
            if acct_move.add_transport_complement and acct_move.cfdi_transport_complement and acct_move.type == "out_invoice":
                move_obj.validate_items(acct_move.items_ids)
                move_obj.validate_ubications(acct_move.ubication_ids)
                move_obj.validate_personal_transport(acct_move.operator_ids)
                move_obj.validate_vehicle(acct_move.vehicle_ids)
        return super(AccountMove, self).post()
    
    @api.constrains("ubication_ids")
    def check_ubications_code(self):
        if len(self.ubication_ids) > 2:
            for ubication in self.ubication_ids:
                if ubication.code_ubication == "" or not ubication.code_ubication:
                    raise ValidationError(
                        _(
                            "CP:Ubicaciones - Si se cuenta con más de dos ubicaciones es necesario ingresar el ID Ubicación."
                        )
                    )
    
    @api.constrains("items_ids")
    def check_quantity(self):
        if len(self.items_ids) > 0 and len(self.ubication_ids) > 2:
            for item in self.items_ids:
                total = 0
                for item_trasport in item.item_ubication_ids:
                    total += item_trasport.quantity
                if item.quantity != total:
                    raise ValidationError(
                        _(
                            "CP:Mercancia - La suma de las cantidades por ubicación no concuerda con la cantidad a trasladar. Favor de revisar. "
                        )
                    )
        return True

    def get_last_modified_attachment(self, name):
        attachment = self.env["ir.attachment"].search(
            [
                ("res_model", "=", self._name),
                ("res_id", "=", self.id),
                ("name", "=like", name),
            ],
            limit=1,
            order="write_date DESC",
        )
        return attachment

    def server_to_local_timestamp(
        self, fecha, dst_tz_name, tz_offset=True, ignore_unparsable_time=True
    ):
        if not fecha:
            return False

        res = fecha
        server_tz = "UTC"
        try:
            # dt_value needs to be a datetime object (so no time.struct_time or mx.DateTime.DateTime here!)
            dt_value = fecha
            if tz_offset and dst_tz_name:
                try:
                    src_tz = pytz.timezone(server_tz)
                    dst_tz = pytz.timezone(dst_tz_name)
                    src_dt = src_tz.localize(dt_value, is_dst=True)
                    dt_value = src_dt.astimezone(dst_tz)
                except Exception:
                    pass
            res = dt_value.replace(tzinfo=None)
        except Exception:
            if not ignore_unparsable_time:
                return False
        return res

    @api.onchange("partner_id")
    def update_checkbox(self):
        if self.partner_id.add_transport_complement:
            self.cfdi_transport_complement = True
        else:
            self.cfdi_transport_complement = False

class AccountEdiFormat(models.Model):
    _inherit = "account.edi.format"
    def _l10n_mx_edi_post_invoice_pac(self, invoice, res):
        move_obj = self.env["account.move"]
        for acct_move in invoice:
            if acct_move.add_transport_complement and acct_move.cfdi_transport_complement and acct_move.type_name == "Factura":
                move_obj.validate_items(acct_move.items_ids)
                move_obj.validate_ubications(acct_move.ubication_ids)
                move_obj.validate_personal_transport(acct_move.operator_ids)
                move_obj.validate_vehicle(acct_move.vehicle_ids)
        if AccountMove.add_transport_complement and AccountMove.cfdi_transport_complement:
            if invoice.items_ids:
                AccountMove.generate_transport_complement(self, invoice, res)
        return super(AccountEdiFormat,self)._l10n_mx_edi_post_invoice_pac(invoice, res)
