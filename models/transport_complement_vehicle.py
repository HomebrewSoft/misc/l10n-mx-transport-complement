from odoo import fields, models, tools, _, api
from odoo.exceptions import ValidationError 

import re

class Transport_Complement_Vehicle(models.Model):
    _name="l10n_mx_transport_complement_vehicle"

    name = fields.Char(
        required=True,
        string="Descripción",
    )
    permSCT = fields.Many2one(
        comodel_name="sat_permsct_catalog",
        required=True,
        string="Permiso SCT",
    )
    numpermSCT = fields.Char(
        required=True,
        string="Número Permiso SCT",
    )
    nameInsurance = fields.Char(
        required=True,
        string="Nombre Aseguradora",
    )
    numPolicy = fields.Char(
        required=True,
        string="Número de poliza",
    )
    vehicleConfig = fields.Many2one(
        comodel_name="sat_config_transport_catalog",
        required=True,
        string="Configuración Vehicular",
    )
    vehicleLicense = fields.Char(
        required=True,
        string="Placas",
    )
    vehicleYear = fields.Char(
        size=4,
        required=True,
        string="Año del vehiculo",
    )

    @api.constrains("vehicleYear")
    def _check_vehicle_year(self):
        if self.vehicleYear:
            pattern = "(19[0-9]{2}|20[0-9]{2})"
            if re.match(pattern, self.vehicleYear):
                return True
            else:
                raise ValidationError(
                    _(
                        "CCT:AnioModeloVM - Solo se permiten caracteres númericos."
                    )
                )
        return False
    
    @api.constrains("vehicleLicense")
    def _check_vehicleLicense(self):
        if self.vehicleLicense:
            pattern = "[^(?!.*\s)-]{6,7}"
            if re.match(pattern, self.vehicleLicense):
                return True
            else:
                raise ValidationError(
                    _(
                        "CCT:PlacaVM - Solo se permiten caracteres alfanúmericos y debe contener una longitud de 6 a 7."
                    )
                )
        return False
    
    @api.constrains("numPolicy")
    def _check_numPolicy(self):
        self.check_pattern(self.numPolicy, 3, 30, "NumPolizaSeguro")
    
    @api.constrains("nameInsurance")
    def _check_nameInsurance(self):
        self.check_pattern(self.nameInsurance, 3, 50, "NombreAseg")

    @api.constrains("numpermSCT")
    def _check_numpermSCT(self):
        self.check_pattern(self.numpermSCT, 1, 50, "NumPermisoSCT")

    def check_pattern(self, text, minimum, maximum, type_field_cce):
        if text:
            pattern = "^[^|]{" + str(minimum) + "," + str(maximum) + "}$"
            if re.match(pattern, text):
                return True
            else:
                raise ValidationError(
                    _(
                        "CCE:"
                        + type_field_cce
                        + " - Solo se permiten caracteres alfanúmericos y debe contener una longitud de "
                        + str(minimum)
                        + " a "
                        + str(maximum)
                        + " caracteres"
                    )
                )
        return False
